import pytest
from pytest import raises
import os
from unittest.mock import MagicMock

# A Duck number is a positive number which has zeroes present in it, For example 3210, 8050896, 70709 are all Duck
# numbers. Please note that a numbers with only leading 0s is not considered as Duck Number. For example,
# numbers like 035 or 0012 are not considered as Duck Numbers. A number like 01203 is considered as Duck because
# there is a non-leading 0 present in it.

class TestDuckNumber:


    @pytest.fixture()
    def mock_open(self,monkeypatch):
        mock_file = MagicMock()  # crate mock file object
        mock_file.readline = MagicMock(return_value="test file reading")  # mock reading of file
        mock_open = MagicMock(return_value=mock_file)  # Attach mock_open to mock_file
        monkeypatch.setattr("builtins.open",mock_open)  # attach mock_open function to mock system file  open() function for reading
        return mock_open

    def ReadFile(self,filename):
        if not os.path.exists(filename):
            raise Exception("File Not Exit")
        file=open(filename,"r")
        line=file.readline()
        return line

    def test_CorrectFileRead(self,mock_open,monkeypatch):
        mock_exits = MagicMock(return_value=True)  #
        monkeypatch.setattr("os.path.exists", mock_exits)
        result=self.ReadFile("input.txt")
        mock_open.assert_called_once_with("input.txt","r") # ReadFile called only once
        assert result=="test file reading"

    def test_Filenotexist(self,mock_open,monkeypatch):
        mock_exits=MagicMock(return_value=False) #
        monkeypatch.setattr("os.path.exists", mock_exits)
        with raises(Exception):
            self.ReadFile("input.txt")

    def check_duck(self,s):
        i=0
        n=len(s)
        while(i<n and s[i]=='0'):
            i=i+1
        while(i<n):
            if(s[i]=='0'):
                return 1
            i=i+1
        return 0

    def test_duck(self):
        assert self.check_duck("0")==0
        assert self.check_duck("123")==0
        assert self.check_duck("23064")==1
        assert self.check_duck("00123")==0
        assert self.check_duck("0123402")==1
        assert self.check_duck("45900")==1













