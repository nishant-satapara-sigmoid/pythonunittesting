class DuckNumber:

    def check_duck():
        try:
            file = open('input.txt', 'r')
            for s in file:
                i = 0
                n = len(s)
                flag = False
                while (i < n and s[i] == '0'):
                    i = i + 1
                while (i < n):
                    if (s[i] == '0'):
                        flag = True
                    i = i + 1
                if (flag):
                    print(f"{s} is DuckNumber")
                else:
                    print(f"{s} is not a DuckNumber")

            file.close()
        except:
            print("file not open")

    if __name__ == "__main__":
        check_duck()
